class generateGame{
    constructor(playerChoice, computerChoice){
        this.playerChoice = playerChoice;
        this.computerChoice = computerChoice
    }
    compareHands(){
        if (this.playerChoice == this.computerChoice) {
            return 'DRAW'
        } else if ((this.playerChoice == 'batu' && this.computerChoice == 'batu') || (this.playerChoice == 'gunting' && this.computerChoice == 'kertas') || (this.playerChoice == 'kertas' && this.computerChoice == 'batu')) {
            return 'PLAYER 1 WIN!'
        } else {
            return 'COM WIN!'
        }
    }
    

}
function mulaiGame() {
    const options = document.querySelectorAll('.player-1 li img')
    options.forEach(function(playerIn) {
        playerIn.addEventListener('click', function() {
            resetColor();
            const playerOne = playerIn.className;
            document.querySelector('.player-1 .'+ playerOne).style.backgroundColor = '#C4C4C4';
            const refresh = document.querySelector('.refresh img')
            refresh.addEventListener('click', function(){
                refreshGame();
            })
            const computer = computerGenerator();
            const games = new generateGame(playerOne, computer);
            const hasil = games.compareHands();
            document.getElementById('gameResult').style.visibility = 'visible';
            document.getElementById('vs').style.visibility = 'hidden';
            document.getElementById('result').innerHTML = hasil;
            console.log('Hasil: '+hasil);
            }
        )     
    }
)
}
function resetColor(){
    document.querySelector('.player-1 .batu').style.backgroundColor = "#9c835f";
    document.querySelector('.player-1 .gunting').style.backgroundColor = "#9c835f";
    document.querySelector('.player-1 .kertas').style.backgroundColor = "#9c835f";
    document.querySelector('.computer .batu').style.backgroundColor = "#9c835f";
    document.querySelector('.computer .gunting').style.backgroundColor = "#9c835f";
    document.querySelector('.computer .kertas').style.backgroundColor = "#9c835f";
}

function refreshGame() {
    document.getElementById('gameResult').style.visibility = 'hidden';
    document.getElementById('vs').style.visibility = 'visible';
    resetColor();
}

function computerGenerator() {
    let hasil = null;
    const comp = Math.floor(Math.random()*10);
    if (comp <= 3) {
        hasil = 'batu';
    } else if (comp > 3 && comp <= 6) {
        hasil = 'kertas';
    } else {
        hasil = 'gunting';
    }
    document.querySelector('.computer .'+hasil).style.backgroundColor = '#C4C4C4';
    return hasil;
}

mulaiGame()