const express = require("express");
const app = express();
const fs = require("fs");
const sessionLog = require("./login-session.json");
const usrData = require("./users.json");


app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.use(express.static("./public"));

app.set("view engine", "ejs");

const authLogin = (req,res,next) => {
  const {username, password} = req.body;
  const authMatch = usrData.find((row) => row.username == username && row.password == password);
  if (authMatch) {
    next();  
  } else { 
    res.redirect("/login")
  };
};

app.get("/", (req,res) => {
  const {username} = req.params;
  res.render("index.ejs", {username});
});

app.get("/login", (req,res) => {
  res.render("login.ejs");
});

app.get("/game", authLogin, (req,res) => {
  res.render("games.ejs");
});


app.post("/login", authLogin, (req,res) => {
  const {username} = req.body;
  const users = {username};
  fs.writeFileSync("./login-session.json", JSON.stringify(users));
  res.json({
    message : "Login Success"
  });
});

app.get("/users", (req,res) => {
  res.send(data);
});

app.use((err,req,res,next) => {
  res.status(404).render("404.ejs");
});


app.listen(4000, () => console.log(`Listening at http://localhost:${4000}`));